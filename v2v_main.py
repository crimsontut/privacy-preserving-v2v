import copy
from typing import List, Tuple
import numpy as np
import os
import sys
from tabulate import tabulate
import v2v_algo
import time
import random
import pickle
import json
import glob

class V2vMain:
    class DemanderCfg:
        def __init__(self, map_row_idx, map_col_idx, requested_quantity_kw, secret_key):
            self.map_row_idx = map_row_idx
            self.map_col_idx = map_col_idx
            self.requested_quantity_kw = requested_quantity_kw
            self.secret_key = secret_key

    class SupplierCfg:
        def __init__(self, map_row_idx, map_col_idx, offered_price_per_kw, secret_key):
            self.map_row_idx = map_row_idx
            self.map_col_idx = map_col_idx
            self.offered_price_per_kw = offered_price_per_kw
            self.secret_key = secret_key

    def __init__(self, cfg_dict, kdc=None, in_simulation_mode=False):
        self.is_privacy_protection_enabled = cfg_dict['is_privacy_protection_enabled']
        self.generated_rand_num_start = cfg_dict['generated_rand_num_start']
        self.generated_rand_num_end = cfg_dict['generated_rand_num_end']
        self.map_num_rows = cfg_dict['map_num_rows']
        self. map_num_cols = cfg_dict['map_num_cols']

        if not kdc:
            # Generate Keys by the KDC and distribute them to the server, the demanders, and the suppliers
            self.kdc = v2v_algo.KDC()
            self.kdc.gen_keys(n=(self.map_num_rows * self.map_num_cols) + 1, num_demanders=len(cfg_dict['demanders']), \
                num_suppliers=len(cfg_dict['suppliers']), generated_rand_num_start=self.generated_rand_num_start,
                         generated_ran_num_end=self.generated_rand_num_end, in_simulation_mode=in_simulation_mode)  # generated
            # keys are inside the kdc object
        else:
            self.kdc = kdc

        demanders_secret_keys, suppliers_secret_keys, kdc_shared_split_vec = self.kdc.distribute_keys()
        if in_simulation_mode:
            demanders_secret_keys = [demanders_secret_keys[0]] * len(cfg_dict['demanders']) # A hack for all
            # demanders to use the same key in simulation mode to save time
            suppliers_secret_keys = [suppliers_secret_keys[0]] * len(cfg_dict['suppliers']) # A hack for all
            # suppliers to use the same key in simulation mode to save time
        self.demanders_cfgs_list: List[V2vMain.DemanderCfg] = self.__create_demanders_cfgs_list(cfg_dict['demanders'], demanders_secret_keys)
        self.suppliers_cfgs_list: List[V2vMain.SupplierCfg] = self.__create_suppliers_cfgs_list(cfg_dict['suppliers'], suppliers_secret_keys)
        self.kdc_shared_split_vec = kdc_shared_split_vec

    def __create_demanders_cfgs_list(self, demanders_cfgs_list, demanders_secret_keys) -> List[DemanderCfg]:
        demanders_ret_list: List[V2vMain.DemanderCfg] = []
        for a_dict, secret_key in zip(demanders_cfgs_list, demanders_secret_keys):
            demander_cfg = V2vMain.DemanderCfg(a_dict['row_idx'], a_dict['col_idx'], a_dict['requested_quantity_kw'],
                                               secret_key)
            demanders_ret_list.append(demander_cfg)
        return demanders_ret_list

    def __create_suppliers_cfgs_list(self, suppliers_cfgs_list, suppliers_secret_keys) -> List[SupplierCfg]:
        suppliers_ret_list: List[V2vMain.SupplierCfg] = []
        for a_dict, secret_key in zip(suppliers_cfgs_list, suppliers_secret_keys):
            supplier_cfg = V2vMain.SupplierCfg(a_dict['row_idx'], a_dict['col_idx'], a_dict['offered_price_per_kw'],
                                               secret_key)
            suppliers_ret_list.append(supplier_cfg)
        return suppliers_ret_list

    def send_encrypt_demanders_requests(self) -> Tuple[List[v2v_algo.Demander], float]:
        """Each demander encrypts each request and send it to the server if the encryption feature is enabled,
        or it sends the request as it is to the server otherwise. The server combines all these requests in a
           list to be processed in this round.
           Returns:
               - List of Demanders encrypted requests
               - Time taken by the demanders in ms
        """
        tic = time.time()
        demanders_list: List[v2v_algo.Demander] = []
        for a_demander_cfg in self.demanders_cfgs_list:
            if self.is_privacy_protection_enabled:
                demander = v2v_algo.Demander(demander_secret_key=a_demander_cfg.secret_key,
                                             s=self.kdc_shared_split_vec,
                                             map=np.zeros((self.map_num_rows, self.map_num_cols)),
                                             curr_pos=(a_demander_cfg.map_row_idx, a_demander_cfg.map_col_idx))
                demander.flaten_map(a_demander_cfg.requested_quantity_kw) # The map for that demander is flattened inside
                # the demander object
                demander.split(0)
                demander.encrypt()  # encrypt to generate q`, q`` inside the demander object
                demanders_list.append(demander)
            else:
                demanders_list.append(a_demander_cfg)
        toc = time.time()
        time_taken_in_ms = (1000 * (toc - tic))
        print("Overall Time taken by demanders to encrypt their requests is: ", str(time_taken_in_ms), "ms")
        avg_time_taken_in_ms = time_taken_in_ms / len(self.demanders_cfgs_list)
        print("Average Time taken by demanders to encrypt their requests is: ", str(avg_time_taken_in_ms), "ms")
        return demanders_list, avg_time_taken_in_ms

    def send_encrypt_suppliers_requests(self) -> Tuple[List[v2v_algo.Supplier], float]:
        """Each supplier encrypts each request and send it to the server if the encryption feature is enabled,
        or it sends the request as it is to the server otherwise. The server combines all these requests in a
           list to be processed in this round.
           Returns:
               - List of Suppliers encrypted requests
               - Time taken by the suppliers in ms
        """
        tic = time.time()
        suppliers_list: List[v2v_algo.Supplier] = []
        for a_supplier_cfg in self.suppliers_cfgs_list:
            if self.is_privacy_protection_enabled:
                supplier = v2v_algo.Supplier(supplier_secret_key=a_supplier_cfg.secret_key,
                                             s=self.kdc_shared_split_vec,
                                             map=np.zeros((self.map_num_rows, self.map_num_cols)),
                                             curr_pos=(a_supplier_cfg.map_row_idx, a_supplier_cfg.map_col_idx),
                                             price_per_kw=a_supplier_cfg.offered_price_per_kw)
                supplier.calculate_cost()
                supplier.flaten_map(a_supplier_cfg.offered_price_per_kw) # The map for that supplier is
                # flattened inside
                # the supplier object
                supplier.split(1)
                supplier.encrypt()  # encrypt to generate q`, q`` inside the supplier object
                suppliers_list.append(supplier)
            else:
                suppliers_list.append(a_supplier_cfg)
        toc = time.time()
        time_taken_in_ms = (1000 * (toc - tic))
        print("Overall Time taken by suppliers to encrypt their requests is: ", str(time_taken_in_ms), "ms")
        avg_time_taken_in_ms = time_taken_in_ms / len(self.suppliers_cfgs_list)
        print("Average Time taken by suppliers to encrypt their requests is: ", str(avg_time_taken_in_ms), "ms")
        return suppliers_list, avg_time_taken_in_ms

    def build_final_cost_matrix_and_compute_assignment_by_server(self, demanders_list: List[v2v_algo.Demander],
                                                          suppliers_list: List[v2v_algo.Supplier],
                                                                 hungarian_execution_time_ms: None):
        """
        The server computes the cost matrix. This function can run in two modes:
         - The first if the privacy preserving mode is enabled, where the server runs the operations blindly,
         without knowing the locations of the users
         - The second if the privacy preserving mode is disabled, were the server has access to the locations of all
         users
         - hungarian_execution_time_ms: Is the time taken by the Hungarian algorithm in a previous run to compare with a
    baseline (with/without) including the Hungarian execution time behavior. It is disabled by default.
         Returns:
         - The output is the same in both cases, as it will generate the 2D matrix that represent the cost between
         each demander and each supplier, which size is (DxS), where D is the number of demanders and S is the number of suppliers
         - 2D assignment matrix that represents the assignment of each demander to which supplier by running the
         Hungarian algorithm -> Does not depend on privacy preserving flag, as it runs the same way in both cases
         - Time taken by the server in ms
        """
        server_x_inv, server_y_inv = self.kdc.get_server_key()
        server = v2v_algo.Server(server_x_inv, server_y_inv)
        if self.is_privacy_protection_enabled:
            tic = time.time()
            server.pre_process_received_demander_data(demanders_list)  # This function pre-processes the encryption
            # list inside the demander to be efficient when the matching algorithm runs
            print("Time taken by the server to preprocess the requests from the demanders (Not considered part of "
                  "Server Setup): {} ms".format(1000 * (time.time() - tic)))

        tic = time.time()
        if self.is_privacy_protection_enabled:
            server.build_final_matrix(suppliers_list, demanders_list)
            cost_matrix = server.final_matrix.round() # Round to the nearest integer for Hungarian algorithm speed
        else:
            cost_matrix = self.build_cost_matrix_without_privacy_protection(suppliers_list, demanders_list)
            cost_matrix = cost_matrix.round()  # Round to the nearest integer for Hungarian algorithm speed
        cost_matrix_execution_time_ms = 1000 * (time.time() - tic)
        print("Time taken by the server to calculate the Hungarian algorithm: {} ms".format(cost_matrix_execution_time_ms))
        # print(tabulate(cost_matrix, tablefmt="fancy_grid", floatfmt=".2f", maxcolwidths=5, ))
        # total_cost = server.ans
        # print("Total cost: ", total_cost)
        assignation_matrix = None
        if not hungarian_execution_time_ms:
            tic = time.time()
            server.calcualte_assignment(cost_matrix)
            toc = time.time()
            hungarian_execution_time_ms = 1000 * (toc - tic)
            print("Time taken by the server to calculate the Hungarian algorithm: {} ms".format(hungarian_execution_time_ms))
            assignation_matrix = server.ans_mat
        else:
            print("Time taken by the server (provided, not calculated) to calculate the Hungarian algorithm: {} "
                  "ms".format(hungarian_execution_time_ms))

            # print(tabulate(assignation_matrix, tablefmt="fancy_grid", floatfmt=".2f", maxcolwidths=5, ))
        time_taken_in_ms = cost_matrix_execution_time_ms + hungarian_execution_time_ms
        print("Overall Time taken by the server to compute the final cost and compute users' assignments is: ",
              str(time_taken_in_ms), "ms")

        return cost_matrix, assignation_matrix, time_taken_in_ms

    @staticmethod
    def build_cost_matrix_without_privacy_protection(suppliers_list: List[SupplierCfg], demanders_list: List[
        DemanderCfg]) -> np.ndarray:
        """
        This function  computes the cost matrix by the server without any privacy protection, which means the server
        has access to both the suppliers' and the demanders' locations
        Returns:
            - Cost Matrix
        """
        cost_matrix = np.zeros((len(demanders_list), len(suppliers_list)))
        for i, a_demander_cfg in enumerate(demanders_list):
            for j, a_supplier_cfg in enumerate(suppliers_list):
                euclidean_distance = v2v_algo.Person.calculate_euclidean_distance(a_demander_cfg.map_row_idx,
                                                                                  a_demander_cfg.map_col_idx,
                                                                                  a_supplier_cfg.map_row_idx,
                                                                                  a_supplier_cfg.map_col_idx)
                cost_matrix[i][j] = euclidean_distance + a_demander_cfg.requested_quantity_kw * a_supplier_cfg.offered_price_per_kw
        return cost_matrix


def simulation_run(simulation_dict: dict, v2v_default_cfg_dict: dict, simulation_run_num: int):
    """
    The simulation calls `main_single_run` multiple times with different configurations
    """
    run_hungarian_once_for_privacy_and_no_privacy = simulation_dict['run_hungarian_once_for_privacy_implemented_and_not_implemented']
    tic = time.time()
    diagnosis_map_sizes = []
    diagnosis_num_users = []
    diagnosis_server_exec_time_with_privacy = []
    diagnosis_demanders_encryption_time_with_privacy = []
    diagnosis_suppliers_encryption_time_with_privacy = []

    diagnosis_server_exec_time_without_privacy = []

    kdc_key_for_map_size_str_pattern: str = simulation_dict['serialization_paths']['kdc_key_for_map_size_str_pattern']
    for map_size in range(simulation_dict['map_size']['min'], simulation_dict['map_size']['max'], simulation_dict['map_size']['inc']):
        kdc_key_for_map_size_path = kdc_key_for_map_size_str_pattern.format(map_size)
        if os.path.exists(kdc_key_for_map_size_path):
            print("Importing the stored KDC for map size: {}".format(map_size))
            kdc = pickle.load(open(kdc_key_for_map_size_path, mode='rb'))
            print("Imported the stored KDC for map size: {} successfully".format(map_size))
        else:
            kdc = None
        v2v_cfg_dict = copy.deepcopy(v2v_default_cfg_dict)
        v2v_cfg_dict['map_num_rows'] = map_size
        v2v_cfg_dict['map_num_cols'] = map_size
        for num_users in range(simulation_dict['users_num']['min'], simulation_dict['users_num']['max'],
                               simulation_dict['users_num']['inc']):
            random.seed(10) # To get the same results after each run
            if num_users > (map_size * map_size) / 2:
                break
            diagnosis_map_sizes.append(map_size)
            diagnosis_num_users.append(num_users)
            demanders_list = []
            for _ in range(num_users):
                row_idx, col_idx = find_new_row_idx_and_col_idx(map_size, demanders_list)
                requested_quantity_kw = random.randint(simulation_dict['demanders_random']['min_requested_quantity_kw'],
                                           simulation_dict['demanders_random']['max_requested_quantity_kw'])
                demander = {
                    'row_idx': row_idx,
                    'col_idx': col_idx,
                    'requested_quantity_kw': requested_quantity_kw
                }
                demanders_list.append(demander)
            v2v_cfg_dict['demanders'] = demanders_list

            suppliers_list = []
            for _ in range(num_users):
                row_idx, col_idx = find_new_row_idx_and_col_idx(map_size, suppliers_list)

                requested_quantity_kw = random.randint(simulation_dict['suppliers_random']['min_offered_price_per_kw'],
                                                       simulation_dict['suppliers_random']['max_offered_price_per_kw'])
                supplier = {
                    'row_idx': row_idx,
                    'col_idx': col_idx,
                    'offered_price_per_kw': requested_quantity_kw
                }
                suppliers_list.append(supplier)
            v2v_cfg_dict['suppliers'] = suppliers_list

            hungarian_execution_time = None
            for is_privacy_enabled in [False, True]:
                v2v_cfg_dict['is_privacy_protection_enabled'] = is_privacy_enabled
                disp_v2v_cfg_dict = copy.deepcopy(v2v_cfg_dict)
                disp_v2v_cfg_dict.pop("demanders")
                disp_v2v_cfg_dict.pop("suppliers")
                disp_v2v_cfg_dict['len_demanders'] = len(v2v_cfg_dict['demanders'])
                disp_v2v_cfg_dict['len_suppliers'] = len(v2v_cfg_dict['suppliers'])
                print("Running configurations: ", json.dumps(disp_v2v_cfg_dict, indent=4))
                print('Number of Users: ', len(v2v_cfg_dict['demanders']))
                single_run_ret_dict = main_single_run(v2v_cfg_dict, kdc, in_simulation_mode=True,
                                                      hungarian_execution_time=hungarian_execution_time)
                kdc = single_run_ret_dict['kdc']
                if not os.path.exists(kdc_key_for_map_size_path):
                    pickle.dump(kdc, open(kdc_key_for_map_size_path, 'wb')) # This step is
                    # to save computation time when running on the same map size again
                if is_privacy_enabled:
                    diagnosis_demanders_encryption_time_with_privacy.append(single_run_ret_dict['demanders_time'])
                    diagnosis_suppliers_encryption_time_with_privacy.append(single_run_ret_dict['suppliers_time'])
                    diagnosis_server_exec_time_with_privacy.append(single_run_ret_dict['server_time'])
                else:
                    diagnosis_server_exec_time_without_privacy.append(single_run_ret_dict['server_time'])
                    if run_hungarian_once_for_privacy_and_no_privacy:
                        hungarian_execution_time = single_run_ret_dict['server_time']
                # print(single_run_ret_dict)

            pickle.dump((diagnosis_map_sizes, diagnosis_num_users, diagnosis_server_exec_time_with_privacy,
                         diagnosis_server_exec_time_without_privacy,
                         diagnosis_demanders_encryption_time_with_privacy, diagnosis_suppliers_encryption_time_with_privacy),
                        open('serialize_simulation_results_run_{}.p'.format(simulation_run_num), 'wb'))
            print("Saved the pickle file with the results of the simulation run number: {} and map size: {}".format(simulation_run_num, map_size))
    toc = time.time()
    print("Time taken by the whole simulation run_{} is : {} seconds".format(simulation_run_num, str(toc - tic)))
    return {'diagnosis_map_sizes': diagnosis_map_sizes, 'diagnosis_num_users': diagnosis_num_users,
            'diagnosis_server_exec_time_with_privacy': diagnosis_server_exec_time_with_privacy,
            'diagnosis_server_exec_time_without_privacy': diagnosis_server_exec_time_without_privacy,
            'diagnosis_demanders_encryption_time_with_privacy': diagnosis_demanders_encryption_time_with_privacy,
            'diagnosis_suppliers_encryption_time_with_privacy': diagnosis_suppliers_encryption_time_with_privacy}


def find_new_row_idx_and_col_idx(map_size, demanders_or_suppliers_list):
    find_random_row_col_enabled = True
    row_idx = col_idx = -1
    while find_random_row_col_enabled:
        row_idx = random.randint(0, map_size - 1)
        col_idx = random.randint(0, map_size - 1)
        find_random_row_col_enabled = sum([1 if row_idx == a_user['row_idx'] and col_idx ==
                                                a_user['col_idx'] else 0 for a_user in demanders_or_suppliers_list]) != 0
    return row_idx, col_idx


def main_single_run(v2v_cfg_dict, kdc=None, in_simulation_mode=False,
                    hungarian_execution_time=None):
    """
    hungarian_execution_time: Is the time taken by the Hungarian algorithm in a previous run to compare with a
    baseline (with/without) including the Hungarian execution time behavior. It is disabled by default.
    """
    tic = time.time()
    v2v_main = V2vMain(v2v_cfg_dict, kdc, in_simulation_mode=in_simulation_mode)
    toc = time.time()
    print("Time taken to setup the system: ", (toc - tic), " seconds")

    # The demanders encrypt their requests and send it to the server
    demanders_list, demanders_taken_time_ms = v2v_main.send_encrypt_demanders_requests()
    demanders_list: List[v2v_algo.Demander] = demanders_list # This line is added to identify the return type

    # The suppliers encrypt their offers and send it to the server
    suppliers_list, suppliers_taken_time_ms = v2v_main.send_encrypt_suppliers_requests()
    suppliers_list: List[v2v_algo.Supplier] = suppliers_list # This line is added to identify the return type

    # The server builds the cost matrix internally
    cost_matrix, assignation_matrix, server_taken_time_ms = \
        v2v_main.build_final_cost_matrix_and_compute_assignment_by_server(demanders_list, suppliers_list, hungarian_execution_time)

    toc = time.time()
    print("Time taken by a single simulation run number {}: {} seconds".format(simulation_run_num, (toc - tic)))
    print("===========================================================================================================")
    return {'cost_matrix': cost_matrix, 'assignation_matrix': assignation_matrix, 'server_time': server_taken_time_ms,
            'demanders_time': demanders_taken_time_ms, 'suppliers_time': suppliers_taken_time_ms, 'kdc': v2v_main.kdc}


def get_average_results(sim_ret_dicts_list: dict):
    combined_server_exec_time_with_privacy_list = []
    combined_server_exec_time_without_privacy_list = []
    combined_demander_enc_time_list = []
    combined_supplier_enc_time_list = []
    for sim_run_num in range(len(sim_ret_dicts_list)):
        combined_server_exec_time_with_privacy_list.append(sim_ret_dicts_list[sim_run_num][
            'diagnosis_server_exec_time_with_privacy'])
        combined_server_exec_time_without_privacy_list.append(sim_ret_dicts_list[sim_run_num][
            'diagnosis_server_exec_time_without_privacy'])
        combined_demander_enc_time_list.append(sim_ret_dicts_list[sim_run_num][
            'diagnosis_demanders_encryption_time_with_privacy'])
        combined_supplier_enc_time_list.append(sim_ret_dicts_list[sim_run_num][
            'diagnosis_suppliers_encryption_time_with_privacy'])
    combined_server_exec_time_with_privacy_list = np.array(combined_server_exec_time_with_privacy_list)
    combined_server_exec_time_without_privacy_list = np.array(combined_server_exec_time_without_privacy_list)
    combined_demander_enc_time_list = np.array(combined_demander_enc_time_list)
    combined_supplier_enc_time_list = np.array(combined_supplier_enc_time_list)

    avg_server_exec_time_with_privacy_list = np.mean(combined_server_exec_time_with_privacy_list, axis=0)
    avg_server_exec_time_without_privacy_list = np.mean(combined_server_exec_time_without_privacy_list, axis=0)
    avg_demander_enc_time_list = np.mean(combined_demander_enc_time_list, axis=0)
    avg_supplier_enc_time_list = np.mean(combined_supplier_enc_time_list, axis=0)

    diagnosis_map_sizes = sim_ret_dicts_list[0]['diagnosis_map_sizes']
    diagnosis_num_users = sim_ret_dicts_list[0]['diagnosis_num_users']

    pickle.dump((diagnosis_map_sizes, diagnosis_num_users, avg_server_exec_time_with_privacy_list,
                 avg_server_exec_time_without_privacy_list,
                 avg_demander_enc_time_list, avg_supplier_enc_time_list),
                open('serialize_simulation_results_avg.p', 'wb'))
    return {
            'diagnosis_map_sizes': diagnosis_map_sizes,
            'diagnosis_num_users': diagnosis_num_users,
            'diagnosis_server_exec_time_with_privacy': avg_server_exec_time_with_privacy_list,
            'diagnosis_server_exec_time_without_privacy': avg_server_exec_time_without_privacy_list,
            'diagnosis_demanders_encryption_time_with_privacy': avg_demander_enc_time_list,
            'diagnosis_suppliers_encryption_time_with_privacy': avg_supplier_enc_time_list
            }

def get_start_simulation_run_index(V2V_SIMULATION_DICT):
    """
    In case 'simulation_resume' is set to True, this function checks the last simulation pickle file saved to the desk,
    and start from their.
    """
    if V2V_SIMULATION_DICT['simulation_resume']:
        # serialize_simulation_results_run_{}.p
        saved_simulation_results_file_names = glob.glob('serialize_simulation_results_run_*.p')
        indices = [int(file_name.split('.')[0].split('_')[-1]) for file_name in saved_simulation_results_file_names]
        simulation_run_start_idx = max(indices)
    else:
        simulation_run_start_idx = 0
    return simulation_run_start_idx


if __name__ == '__main__':
    from v2v_main_cfg import V2V_CFG_DICT as V2V_DEFAULT_CFG_DICT
    from v2v_main_cfg import V2V_SIMULATION_DICT

    if V2V_SIMULATION_DICT['is_simulation_enabled']:
        sim_ret_dicts_list = []
        start_simulation_run_idx = get_start_simulation_run_index(V2V_SIMULATION_DICT)
        for simulation_run_num in range(start_simulation_run_idx, V2V_SIMULATION_DICT['repeat_simulation_counter']):
            sim_ret_dict = simulation_run(V2V_SIMULATION_DICT, V2V_DEFAULT_CFG_DICT, simulation_run_num)
            sim_ret_dicts_list.append(sim_ret_dict)

        average_sim_result_dict = get_average_results(sim_ret_dicts_list)

    else:
        main_single_run(V2V_DEFAULT_CFG_DICT)
    print("Program Completed Successfully")
