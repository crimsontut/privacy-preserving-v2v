import pickle
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
from scipy.interpolate import splprep, splev

GRAPH_COLORS = ['b', 'g', 'r', 'c', 'm', 'y', 'k', '']
GRAPH_LINE_SHAPES = ['-', '--', '-.', ':.']
GRAPH_POINTS_SHAPES = ['.', ',', 'o', 'v', '^', '<', '>']


def find_average(lst):
    return sum(lst) / len(lst)


def print_latex_table_values_for_demander_suppliers_exec_time(selected_map_sizes_list,
                                                              map_sizes_list,
                                                              demanders_execution_times_list,
                                                              suppliers_execution_times_list):
    print("********************************************************************")
    selected_map_sizes_list = [m * m for m in selected_map_sizes_list]
    new_demanders_execution_times_list = []
    new_suppliers_execution_times_list = []
    for idx, map_size in enumerate(map_sizes_list):
        if map_size in selected_map_sizes_list:
            new_demanders_execution_times_list.append(demanders_execution_times_list[idx])
            new_suppliers_execution_times_list.append(suppliers_execution_times_list[idx])

    map_sizes_rep_str = "& {}" * len(selected_map_sizes_list)
    print("\\shortstack{Map \\\\ Size} " + map_sizes_rep_str.format(*selected_map_sizes_list) + " \\\\ \n\\hline")

    demanders_rep_str = "& {:.3f}" * len(selected_map_sizes_list)
    print(r"\shortstack{Demander's \\ Execution \\ Time} " + demanders_rep_str.format(*new_demanders_execution_times_list) +
          " \\\\ \n\\hline")

    suppliers_rep_str = "& {:.3f}" * len(selected_map_sizes_list)
    print(r"\shortstack{Supplier's \\ Execution \\ Time} " + suppliers_rep_str.format(
        *new_suppliers_execution_times_list) +
          " \\\\ \n\\hline")

    print("********************************************************************")

def print_latex_table_values_for(map_sizes_list, num_users_list):
    overall_counter = 1
    for num_users in num_users_list:
        cnt = 0
        for map_size in map_sizes_list:
            map_size *= map_size

            for idx, (a_map_size, a_num_users) in enumerate(zip(diagnosis_map_sizes, diagnosis_num_users)):       
                if a_map_size == map_size and num_users == a_num_users:                 
                    num_users_str = a_num_users * 2 if cnt == 0 else '\,' # Space in latex
                    overhead = diagnosis_server_exec_time_with_privacy[idx] - diagnosis_server_exec_time_without_privacy[idx]
                    overhead_percentage = 100 * overhead / diagnosis_server_exec_time_without_privacy[idx]
                    print("{} & {} & {:.3f} & {:.3f} & {:.3f} & {:.1f}\% \\\\".format(num_users_str,
                                                                             a_map_size,
                                                                             diagnosis_server_exec_time_with_privacy[idx],
                                                                             diagnosis_server_exec_time_without_privacy[idx], overhead, overhead_percentage))
                    cnt += 1
                    overall_counter += 1
                    break


def categorize_average_encryption_time_per_map_size(encryption_times_list, map_sizes_list, num_users_list):
    encryption_times_per_map_size_dict = {}
    for map_size in map_sizes_list:
        encryption_times_per_map_size_dict[map_size] = {'time': [], 'num_users': []}

    for a_time, map_size, num_users in zip(encryption_times_list, map_sizes_list, num_users_list):
        encryption_times_per_map_size_dict[map_size]['time'].append(a_time)
        encryption_times_per_map_size_dict[map_size]['num_users'].append(num_users)

    return encryption_times_per_map_size_dict


def get_exec_time_map_size_for_num_users(num_users_to_display, server_exec_time_with_privacy,
                                         server_exec_time_without_privacy, num_users, map_sizes):
    displayed_3d_data_dict = {'num_users': [], 'map_sizes': [], 'exec_time_with_privacy': [],
                              'exec_time_without_privacy': []}

    server_exec_time_without_privacy_avg_over_num_users = \
        categorize_average_encryption_time_per_map_size(server_exec_time_without_privacy, num_users, map_sizes)
    server_exec_time_with_privacy_avg_over_num_users = \
        categorize_average_encryption_time_per_map_size(server_exec_time_with_privacy, num_users, map_sizes)

    for num_user_to_display in num_users_to_display:
        num_reported_times_for_num_user = len(server_exec_time_with_privacy_avg_over_num_users[num_user_to_display][
                                                  'time'])
        displayed_3d_data_dict['num_users'].extend([num_user_to_display] * num_reported_times_for_num_user)
        displayed_3d_data_dict['map_sizes'].extend(server_exec_time_with_privacy_avg_over_num_users[
                                                       num_user_to_display]['num_users']) # AB: Yes, it is intended to be 'num_users' because the function
        # `categorize_average_encryption_time_per_map_size` was intended to be used only for finding the time per map
        # size
        displayed_3d_data_dict['exec_time_with_privacy'].extend(server_exec_time_with_privacy_avg_over_num_users[
                                                       num_user_to_display]['time'])
        displayed_3d_data_dict['exec_time_without_privacy'].extend(server_exec_time_without_privacy_avg_over_num_users[
                                                                    num_user_to_display]['time'])

    return displayed_3d_data_dict


diagnosis_map_sizes, diagnosis_num_users, diagnosis_server_exec_time_with_privacy, \
    diagnosis_server_exec_time_without_privacy, diagnosis_demanders_encryption_time_with_privacy, diagnosis_suppliers_encryption_time_with_privacy = \
    pickle.load(open('serialize_simulation_results_avg.p', mode='rb'))

diagnosis_map_sizes = [item * item for item in diagnosis_map_sizes]

import numpy as np
server_exec_time_privacy_numpy = np.array(diagnosis_server_exec_time_with_privacy)
server_exec_time_no_privacy_numpy = np.array(diagnosis_server_exec_time_without_privacy)
diff = server_exec_time_privacy_numpy - server_exec_time_no_privacy_numpy

diagnosis_demanders_encryption_time_with_privacy = [item / 1000 for item in diagnosis_demanders_encryption_time_with_privacy]
diagnosis_suppliers_encryption_time_with_privacy = [item / 1000 for item in diagnosis_suppliers_encryption_time_with_privacy]
diagnosis_server_exec_time_with_privacy = [item / 1000 for item in diagnosis_server_exec_time_with_privacy]
diagnosis_server_exec_time_without_privacy = [item / 1000 for item in diagnosis_server_exec_time_without_privacy]

demanders_encryption_time_with_privacy_averaged_over_map_size = \
categorize_average_encryption_time_per_map_size(diagnosis_demanders_encryption_time_with_privacy, diagnosis_map_sizes, diagnosis_num_users)

suppliers_encryption_time_with_privacy_averaged_over_map_size = \
categorize_average_encryption_time_per_map_size(diagnosis_suppliers_encryption_time_with_privacy, diagnosis_map_sizes, diagnosis_num_users)

table_map_sizes = [10, 40, 80]
table_num_users = [10, 50, 110]
print_latex_table_values_for(table_map_sizes, table_num_users)

# max_value_in_demanders_enc_time = max(diagnosis_demanders_encryption_time_with_privacy)
# max_value_in_suppliers_enc_time = max(diagnosis_suppliers_encryption_time_with_privacy)
# max_value_in_demander_suppliers_enc_time = max(max_value_in_demanders_enc_time, max_value_in_suppliers_enc_time) + 0.5
# # Encryption time for demanders as a function of map size and number of users
# fig =plt.figure()
# plt.xlabel('Number of Users')
# plt.ylabel('Execution Time (s)')
# # plt.title('Encryption Execution Time for Demanders')
# # plt.xlim([0, len(list(set(diagnosis_num_users)))]) # Adjust the range for the x-axis
# plt.ylim([0, max_value_in_demander_suppliers_enc_time]) # Adjust the range for the y-axis
# for idx, map_size in enumerate(demanders_encryption_time_with_privacy_averaged_over_map_size):
#     if idx % 2 != 0: # Take one and drop one
#         continue
#     graph_color = GRAPH_COLORS[idx % len(GRAPH_COLORS)]
#     demander_enc_time_per_users = demanders_encryption_time_with_privacy_averaged_over_map_size[map_size]['time']
#     plotted_map_sizes_list = demanders_encryption_time_with_privacy_averaged_over_map_size[map_size]['num_users']
#     plt.plot(plotted_map_sizes_list, demander_enc_time_per_users, '{}x-'.format(graph_color), label='Map Size: {}'.format(map_size))
# plt.legend()
# fig.show()

# Encryption time for suppliers as a function of map size and number of users
fig =plt.figure()
plt.xlabel('Map Size')
plt.ylabel('Execution Time (s)')
# plt.title('Encryption Execution Time for Suppliers')
# plt.xlim([0, len(list(set(diagnosis_num_users)))]) # Adjust the range for the x-axis
# plt.ylim([0, max_value_in_demander_suppliers_enc_time]) # Adjust the range for the y-axis
draw_supplier_enc_time_list = []
draw_demander_enc_time_list = []
draw_map_sizes_list = []
for idx, map_size in enumerate(suppliers_encryption_time_with_privacy_averaged_over_map_size):
    # graph_color = GRAPH_COLORS[idx % len(GRAPH_COLORS)]
    supplier_enc_time = find_average(suppliers_encryption_time_with_privacy_averaged_over_map_size[
                                                   map_size]['time'])
    demander_enc_time = find_average(demanders_encryption_time_with_privacy_averaged_over_map_size[
                                                   map_size]['time'])
    draw_supplier_enc_time_list.append(supplier_enc_time)
    draw_demander_enc_time_list.append(demander_enc_time)
    # plotted_map_sizes_list = suppliers_encryption_time_with_privacy_averaged_over_map_size[map_size]['num_users']
    draw_map_sizes_list.append(map_size)
plt.plot(draw_map_sizes_list, draw_demander_enc_time_list, 'ro-', label='Demanders')
plt.plot(draw_map_sizes_list, draw_supplier_enc_time_list, 'gx-', label='Suppliers')
plt.legend()
fig.show()

print_latex_table_values_for_demander_suppliers_exec_time([20, 30, 40], draw_map_sizes_list,
                                                          draw_demander_enc_time_list,
                                                          draw_supplier_enc_time_list)

server_exec_time_with_privacy_avg_over_map_size = \
categorize_average_encryption_time_per_map_size(diagnosis_server_exec_time_with_privacy, diagnosis_map_sizes, diagnosis_num_users)
server_exec_time_without_privacy_avg_over_map_size = \
categorize_average_encryption_time_per_map_size(diagnosis_server_exec_time_without_privacy, diagnosis_map_sizes, diagnosis_num_users)

max_value_server_exec_time_with_privacy = max(diagnosis_server_exec_time_with_privacy)
max_value_server_exec_time_without_privacy = max(diagnosis_server_exec_time_without_privacy)
max_value_server_exec_time_with_and_without_privacy = max(max_value_server_exec_time_with_privacy,
                                                     max_value_server_exec_time_without_privacy) + 0.5
# Server Execution time with privacy for demander & supplier as a function of number of users
fig =plt.figure()
plt.xlabel('Number of Users')
plt.ylabel('Execution Time (s)')
plt.title('Server Execution Time with Privacy')
# plt.xlim([0, len(list(set(diagnosis_num_users)))]) # Adjust the range for the x-axis
plt.ylim([0, max_value_server_exec_time_with_and_without_privacy]) # Adjust the range for the y-axis
for idx, map_size in enumerate(server_exec_time_with_privacy_avg_over_map_size):
    graph_color = GRAPH_COLORS[idx % len(GRAPH_COLORS)]
    server_time = server_exec_time_with_privacy_avg_over_map_size[map_size]['time']
    plotted_map_sizes_list = server_exec_time_with_privacy_avg_over_map_size[map_size]['num_users']
    plt.plot(plotted_map_sizes_list, server_time, '{}x-'.format(graph_color), label='Map Size: {}'.format(map_size))
plt.legend()
fig.show()

# Server Execution time without privacy for demander & supplier as a function of number of users
fig =plt.figure()
plt.xlabel('Number of Users')
plt.ylabel('Execution Time (s)')
plt.title('Server Execution Time without Privacy')
# plt.xlim([0, len(list(set(diagnosis_num_users)))]) # Adjust the range for the x-axis
plt.ylim([0, max_value_server_exec_time_with_and_without_privacy]) # Adjust the range for the y-axis
for idx, map_size in enumerate(server_exec_time_without_privacy_avg_over_map_size):
    graph_color = GRAPH_COLORS[idx % len(GRAPH_COLORS)]
    server_time = server_exec_time_without_privacy_avg_over_map_size[map_size]['time']
    plotted_map_sizes_list = server_exec_time_without_privacy_avg_over_map_size[map_size]['num_users']
    plt.plot(plotted_map_sizes_list, server_time, '{}x-'.format(graph_color), label='Map Size: {}'.format(map_size))
plt.legend()
fig.show()

# Print in Latex format
# with open('server_exec_time_with_and_without_privacy.txt', 'w') as f:
#     # f.write('Num_users Map_size ')
#     for idx, map_size in enumerate(server_exec_time_without_privacy_avg_over_map_size):
#         server_time_with_privacy = server_exec_time_with_privacy_avg_over_map_size[map_size]['time']
#         server_time_without_privacy = server_exec_time_without_privacy_avg_over_map_size[map_size]['time']
#         num_users = server_exec_time_without_privacy_avg_over_map_size[map_size]['num_users']
#         for a_num_users, a_server_time_with_privacy, a_server_time_without_privacy in zip(num_users,
#                                                                                           server_time_with_privacy,
#                                                                                           server_time_without_privacy):
#             print("({}, {}, {:.3f})".format(a_num_users, map_size, a_server_time_with_privacy))


# Server Execution time for demander & supplier as a function of map size
# fig =plt.figure()
# plt.xlabel('Map Size (MxM)')
# plt.ylabel('Execution Time (s)')
# plt.title('Server Execution Time with/without Privacy')
# plt.plot(diagnosis_map_sizes, diagnosis_server_exec_time_with_privacy, 'go-', label='With Privacy')
# plt.plot(diagnosis_map_sizes, diagnosis_server_exec_time_without_privacy, 'rx-', label='Without Privacy')
# plt.legend()
# fig.show()

displayed_3d_data_dict = get_exec_time_map_size_for_num_users(
                                                            #[15, 35, 55, 75, 95],
                                                            list(set(diagnosis_num_users)),
                                                              diagnosis_server_exec_time_with_privacy,
                                                              diagnosis_server_exec_time_without_privacy,
                                                              diagnosis_num_users, diagnosis_map_sizes)

fig = plt.figure()
ax = plt.axes(projection='3d')

# Data for three-dimensional scattered points
zdata = np.array(displayed_3d_data_dict['exec_time_with_privacy'])
xdata = np.array(displayed_3d_data_dict['map_sizes'])
ydata = np.array(displayed_3d_data_dict['num_users'])
ax.set_xlabel('Map Size')
ax.set_ylabel('Num Users')
ax.set_zlabel('Server Execution Time (s)')
plt.title('Server Execution Time with/without Privacy (s)')
# ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');
ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap=cm.coolwarm, marker='o', label='With Privacy')
# Data for three-dimensional scattered points
zdata = np.array(displayed_3d_data_dict['exec_time_without_privacy'])
xdata = np.array(displayed_3d_data_dict['map_sizes'])
ydata = np.array(displayed_3d_data_dict['num_users'])

ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='viridis', marker='^', label='Without Privacy')
plt.legend(loc="upper right")
fig.show()


plt.rcParams['grid.color'] = (0.5, 0.5, 0.5, 0.1) # fourth parameter is alpha=0.1
fig = plt.figure()
ax: mplot3d.axes3d.Axes3D = plt.axes(projection='3d')
ax.invert_xaxis()
# diagnosis_server_exec_time_with_privacy =
# Data for three-dimensional scattered points
zdata_with_privacy = np.array(displayed_3d_data_dict['exec_time_with_privacy'])
xdata = np.array(displayed_3d_data_dict['map_sizes'])
ydata = np.array(displayed_3d_data_dict['num_users'])

# fit & interpolate the data
tck, u = splprep([xdata, ydata, zdata_with_privacy], k=2, s=10000000) # The higher the value of s the smoother the array
u_fine = np.linspace(0, 1, 1000) # Create more samples and draw the fine-grained curve
x_fine, y_fine, z_fine = splev(u_fine, tck)

ax.set_xlabel('Map Size')
ax.set_ylabel('Num Users')
ax.set_zlabel('Server Execution Time (s)')
# plt.title('Server Execution Time with Privacy (s)')
# trisurf = ax.plot_trisurf(xdata, ydata, zdata_with_privacy, color=(0, 1, 0, 0.1), edgecolor=(0, 1, 0, 0.2)) # Green
# trisurf = ax.plot_trisurf(x_fine, z_fine, z_fine, color=(0, 1, 0, 0.1), edgecolor=(0, 1, 0, 0)) # Green
# trisurf = ax.plot_trisurf(xdata, ydata, zdata_without_privacy, color=(1, 0, 0, 0.1), edgecolor=(1, 0, 0, 0.2)) # Red
trisurf = ax.plot_trisurf(x_fine, y_fine, z_fine, cmap=cm.coolwarm, alpha=0.2)
# fig.colorbar(trisurf, ax=ax, shrink=0.5, aspect=10, label='')
fig.show()


fig = plt.figure()
ax: mplot3d.axes3d.Axes3D = plt.axes(projection='3d')
ax.invert_xaxis()
# diagnosis_server_exec_time_with_privacy =
# Data for three-dimensional scattered points
zdata_without_privacy = np.array(displayed_3d_data_dict['exec_time_without_privacy'])
xdata = np.array(displayed_3d_data_dict['map_sizes'])
ydata = np.array(displayed_3d_data_dict['num_users'])

# fit & interpolate the data
tck, u = splprep([xdata, ydata, zdata_without_privacy], k=2, s=10000000) # The higher the value of s the smoother the
# array. K=2 means that the created equation will be quadratic(ax^2+bx+c)
u_fine = np.linspace(0, 1, 1000) # Create more samples and draw the fine-grained curve
x_fine, y_fine, z_fine = splev(u_fine, tck)

ax.set_xlabel('Map Size')
ax.set_ylabel('Num Users')
ax.set_zlabel('Server Execution Time (s)')
# plt.title('Server Execution Time with Privacy (s)')
# trisurf = ax.plot_trisurf(xdata, ydata, zdata_with_privacy, color=(0, 1, 0, 0.1), edgecolor=(0, 1, 0, 0.2)) # Green
# trisurf = ax.plot_trisurf(x_fine, z_fine, z_fine, color=(0, 1, 0, 0.1), edgecolor=(0, 1, 0, 0)) # Green
# trisurf = ax.plot_trisurf(xdata, ydata, zdata_without_privacy, color=(1, 0, 0, 0.1), edgecolor=(1, 0, 0, 0.2)) # Red
trisurf = ax.plot_trisurf(x_fine, y_fine, z_fine, cmap=cm.coolwarm, alpha=0.2)
# fig.colorbar(trisurf, ax=ax, shrink=0.5, aspect=10, label='')
fig.show()

fig = plt.figure()
ax: mplot3d.axes3d.Axes3D = plt.axes(projection='3d')
ax.invert_xaxis()
overhead = [100 * (server_with_priv_exec_time - server_without_priv_exec_time) / server_without_priv_exec_time for
            server_with_priv_exec_time, server_without_priv_exec_time in zip(displayed_3d_data_dict[
                                                                                 'exec_time_with_privacy'], displayed_3d_data_dict['exec_time_without_privacy'])]
# Data for three-dimensional scattered points
zdata = np.array(overhead)
xdata = np.array(displayed_3d_data_dict['map_sizes'])
ydata = np.array(displayed_3d_data_dict['num_users'])

# fit & interpolate the data
tck, u = splprep([xdata, ydata, zdata], k=2, s=10000000) # The higher the value of s the smoother the
# array. K=2 means that the created equation will be quadratic(ax^2+bx+c)
u_fine = np.linspace(0, 1, 1000) # Create more samples and draw the fine-grained curve
x_fine, y_fine, z_fine = splev(u_fine, tck)

ax.set_xlabel('Map Size')
ax.set_ylabel('Num Users')
ax.set_zlabel('Overhead Percentage')
# plt.title('Server Execution Time without Privacy (s)')
trisurf = ax.plot_trisurf(x_fine, y_fine, z_fine, cmap='YlOrRd', alpha=0.4, linewidth=0, edgecolor=(0, 0, 0, 0))
fig.colorbar(trisurf, ax=ax, shrink=0.5, aspect=10, label='')
plt.show(block=True)
