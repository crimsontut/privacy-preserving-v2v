#!/bin/bash
#SBATCH --job-name=pp_privacy_preserving_job # job Name
#SBATCH --output=%j%x.out # output log file
#SBATCH --error=%j%x.err # error file
#SBATCH --time=24:00:00 # 24 hour of wall time (working time)
#SBATCH --qos=main # "main" for 24 hours, "debug" 15 mins, "long" for 1 week qos, "gpu" for 24 hours GPU partition, "threaded" for 24 hours threaded partition
#SBATCH --nodes=1
#SBATCH --ntasks=1 # 1 CPU core
#SBATCH --mem=32G  # total memory per node
#SBATCH --mail-type=begin        # send email when job begins
#SBATCH --mail-type=end          # send email when job ends
#SBATCH --mail-type=fail         # send email if job fails
#SBATCH --mail-user ambakr@crimson.ua.edu

# Load all required modules below
module load miniconda3

conda activate pp_ridesharing_env
# conda info

# Add lines here to run your GPU-based computations
python v2v_main.py

# srun -p main --qos debug --gres=gpu:1 --mem=32G --time=00:10:00 --pty /bin/bash
# srun -p gpu --qos debug --mem=32G --pty /bin/bash