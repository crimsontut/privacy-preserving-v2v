import numpy as np
import math
import hung_algorithm as hung
"""
    Code Author: @Mahmoud Srewa
    PhD student,  Computer Science Departement, the university of Alabama.
    Written in Alsharif Labs - the university of Alabama
"""
    

class Person:
    """
    a parent class that represents both trusted entities (a user and owner),
    Create this class to avoid duplicating code, as they both share some functions and attributes.
    """ 
    def __init__(self,n: int,m1: np.ndarray,m2: np.ndarray,s : np.ndarray,map: np.ndarray, curr_postion: tuple) -> None:
        # n: int = M x M
        self.m1 = m1
        self.m2 = m2
        self.s = s
        self.map = map # 2D matrix (MxM) that represents the map. Initially filled with zeros
        self.rand_index_1 = np.zeros(n)
        self.rand_index_2= np.zeros(n)
        self.curr_postion = curr_postion

    def flaten_map(self):
        self.map_flatened =self.map.flatten()
        # TODO: AB: Add the quantity in the flattened array


    def split(self,split_bit:int): # split bit for the demander has to be reversed in respect with the supplier. If the demander splitted on 1, the supplier will split on zero
        for pos,value in enumerate(self.s):
                    if value == split_bit:
                        self.rand_index_1[pos] = self.map_flatened[pos]
                        self.rand_index_2[pos] = self.map_flatened[pos]
                    else:
                        if self.map_flatened[pos] !=1 and self.map_flatened[pos] !=0:
                            rand = np.random.uniform(0,self.map_flatened[pos])
                            self.rand_index_1[pos] = rand
                            self.rand_index_2[pos] = self.map_flatened[pos] - rand
                    
                        elif self.map_flatened[pos] ==1: # TODO: AB: This elseif and its content should be removed, as it is part of the if condition above
                            rand = np.random.dirichlet(np.ones(2),size=1)
                            self.rand_index_1[pos] = rand[0][0]
                            self.rand_index_2[pos] = rand[0][1]
                        else:
                            self.rand_index_1[pos] = np.random.randint(50,200)
                            self.rand_index_2[pos] = -self.rand_index_1[pos]
    
    def create_final_index(self,type):
        if type ==0: # If this function is called by the demander
            self.encrypted_index_1 = np.dot(np.transpose(self.m1),self.rand_index_1)
            self.encrypted_index_2 = np.dot(np.transpose(self.m2),self.rand_index_2)
        else: # If this function is called by the supplier
            self.encrypted_index_1 = np.dot(self.m1,self.rand_index_1)
            self.encrypted_index_2 = np.dot(self.m2,self.rand_index_2)


class Demander(Person):
    """
    Owner class contains methods that only used by an owner of a docuemnts (Admin)
    """ 
    def __init__(self,m1,m2,s,map: np.ndarray, curr_pos : tuple) -> None:
        """
        arguments:
           vocab: set of vocab that we do search with, we only can search through
           documents: set of documents that are going to be uploaded to a server
        """
        #m1 = np.random.randint(1,10,(n,n))
        #m2 = np.random.randint(1,10,(n,n))
        #s = np.random.choice([0,1],n)
        Person.__init__(self,map.size,m1,m2,s,map,curr_pos)
        self.map[curr_pos] =1


    @staticmethod
    def gen_keys(n :int):
        # TODO: AB: multiply all the keys together to generate the final m1, m2, so that the rest of the algorithm remains the same
        m1 = np.random.randint(1,5,(n,n))
        m2 = np.random.randint(1,5,(n,n))
        s = np.random.choice([0,1],n)
        return m1,m2,s



class Supplier(Person):
    """
    The User class contains methods that are only used by users, such as initiating query indexes. 
    """ 
    def __init__(self,m1,m2,s,map,curr_pos : tuple) -> None:
       
        Person.__init__(self,map.size,m1,m2,s,map,curr_pos)

    def calculate_cost(self):
        for pos_x in range(self.map.shape[0]):
            for pos_y in range(self.map[pos_x].shape[0]):
                self.map[pos_x,pos_y] =  math.sqrt((self.curr_postion[0] - pos_x)**2 +(self.curr_postion[1] - pos_y)**2)

    

                    
class Server:
    """
    The Server class contains the query processing method, and return the matched documents
    """ 
        
    def build_final_matrix(self,suppliers: list[Supplier] , demanders: list[Demander]):
        self.final_matrix = np.zeros((len(demanders),len(suppliers)))
        #print(self.final_matrix)
        for pos_x in range(len(demanders)):
            for pos_y in range(len(suppliers)):
                
                first_part =np.dot(demanders[pos_x].encrypted_index_1,suppliers[pos_y].encrypted_index_1) 
                sec_part= np.dot(demanders[pos_x].encrypted_index_2,suppliers[pos_y].encrypted_index_2)

                self.final_matrix[pos_x][pos_y] =first_part + sec_part

    def calcualte_assignment(self):
         hungo = hung.Hungarian_algo( self.final_matrix)
         self.ans, self.ans_mat = hungo.run_algo()



        