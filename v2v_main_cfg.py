V2V_SIMULATION_DICT = {
    'is_simulation_enabled': True,
    # The rest of the configurations are relevant if the simulation is True only
    'repeat_simulation_counter': 20, # How many times you want this simulation to be repeated then an average of the
    'simulation_resume': False, # Whether you want the simulation to resume from the last simulation iteration
    # results is reported
    'run_hungarian_once_for_privacy_implemented_and_not_implemented': True, # If this set to true, then the hungarian
    # algorithm will run once when the privacy is not implemented and the same execution time will be reported when
    # the privacy scheme is implemented to eliminate the stochastic behavior in the reported execution time caused by
    # the Hungarian for the comparison's sake.
    'map_size': { # Random configurations for map size
        'min': 10,
        'max': 90,
        'inc': 10
    },
    'users_num': { # Random configurations for users. This is the number of demanders and it is also the same number
        # of suppliers, as our solution assumes that they are the same number
        'min': 10,
        'max': 120,
        'inc': 20
    },
    'demanders_random': { # Random configurations for demanders
        'min_requested_quantity_kw': 2,
        'max_requested_quantity_kw': 200
    },
    'suppliers_random': { # Random configurations for suppliers
        'min_offered_price_per_kw': 2,
        'max_offered_price_per_kw': 200
    },
    'serialization_paths': {
        'kdc_key_for_map_size_str_pattern': 'serialize_kdc_key_for_map_size_{}.p'
    }
}

V2V_CFG_DICT = {
    'is_privacy_protection_enabled': True,
    'generated_rand_num_start': 1,
    'generated_rand_num_end': 5,
    'map_num_rows': 9,
    'map_num_cols': 10,
    'demanders': [
        {
            'row_idx': 0,
            'col_idx': 4,
            'requested_quantity_kw': 2
        },
        {
            'row_idx': 2,
            'col_idx': 2,
            'requested_quantity_kw': 2
        },
        {
            'row_idx': 3,
            'col_idx': 6,
            'requested_quantity_kw': 2
        },
        {
            'row_idx': 4,
            'col_idx': 8,
            'requested_quantity_kw': 2
        },
        {
            'row_idx': 6,
            'col_idx': 4,
            'requested_quantity_kw': 2
        },
        {
            'row_idx': 7,
            'col_idx': 1,
            'requested_quantity_kw': 2
        },
    ],
    'suppliers': [
        {
            'row_idx': 0,
            'col_idx': 0,
            'offered_price_per_kw': 3
        },
        {
            'row_idx': 1,
            'col_idx': 7,
            'offered_price_per_kw': 3
        },
        {
            'row_idx': 2,
            'col_idx': 4,
            'offered_price_per_kw': 3
        },
        {
            'row_idx': 4,
            'col_idx': 0,
            'offered_price_per_kw': 3
        },
        {
            'row_idx': 4,
            'col_idx': 4,
            'offered_price_per_kw': 3
        },
        {
            'row_idx': 5,
            'col_idx': 6,
            'offered_price_per_kw': 3
        }
    ]
}
