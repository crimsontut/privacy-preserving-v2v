import numpy as np
import math
import hung_algorithm as hung
import copy
"""
    Code Author: @Mahmoud Srewa
    PhD student,  Computer Science Departement, the university of Alabama.
    Written in Alsharif Labs - the university of Alabama
"""
    

class Person:
    """
    a parent class that represents both trusted entities (a user and owner),
    Create this class to avoid duplicating code, as they both share some functions and attributes.
    """ 
    def __init__(self, n: int, secret_key: np.ndarray, s: np.ndarray, map: np.ndarray, curr_postion: tuple) -> None:
        # n: int = M x M
        self.secret_key = secret_key
        self.s = s
        self.map = map # 2D matrix (MxM) that represents the map. Initially filled with zeros
        self.rand_index_1 = np.zeros(n)
        self.rand_index_2= np.zeros(n)
        self.curr_postion = curr_postion
        self.encrypted_matrix = None # filled in the child class encrypt function

    def flaten_map(self, pad_quantity: float):
        self.map_flatened =self.map.flatten()
        pad_quantity = np.array([pad_quantity])
        self.map_flatened = np.hstack([self.map_flatened, pad_quantity])

    def split(self,split_bit:int): # split bit for the demander has to be reversed in respect with the supplier. If the demander splitted on 1, the supplier will split on zero
        for pos,value in enumerate(self.s):
                    if value == split_bit:
                        self.rand_index_1[pos] = self.map_flatened[pos]
                        self.rand_index_2[pos] = self.map_flatened[pos]
                    else:
                        if self.map_flatened[pos] !=1 and self.map_flatened[pos] !=0:
                            rand = np.random.uniform(0,self.map_flatened[pos])
                            self.rand_index_1[pos] = rand
                            self.rand_index_2[pos] = self.map_flatened[pos] - rand
                    
                        elif self.map_flatened[pos] ==1: # TODO: AB: This elseif and its content should be removed, as it is part of the if condition above
                            rand = np.random.dirichlet(np.ones(2),size=1)
                            self.rand_index_1[pos] = rand[0][0]
                            self.rand_index_2[pos] = rand[0][1]
                        else:
                            self.rand_index_1[pos] = np.random.randint(50,200)
                            self.rand_index_2[pos] = -self.rand_index_1[pos]
    
    def encrypt(self):
        # Should be implemented in the child class to fill both self.encrypted_matrix
        self.encrypted_matrix = None

    @staticmethod
    def calculate_euclidean_distance(pos1x, pos1y, pos2x, pos2y):
        return math.sqrt((pos1x - pos2x)**2 + (pos1y - pos2y)**2)

class Demander(Person):
    """
    Demander class
    """ 
    def __init__(self, demander_secret_key: list, s, map: np.ndarray, curr_pos : tuple) -> None:
        """
        arguments:
           demander_secret_key: Secret key of the demander. Each component of the list is a numpy array with the size (1, n)
           map: The map
           curr_pos: The current position of the demander in the map
        """
        Person.__init__(self, map.size + 1, demander_secret_key, s, map, curr_pos) # AB: The added one in the first
        # parameter to justify the padded value at the end that represents the quantity to be charged in KW
        self.map[curr_pos] = 1

    def encrypt(self):
        self.encrypted_matrix = []
        self.rand_index_1 = self.rand_index_1[:, None]  # The generated size is (n, 1)
        self.rand_index_2 = self.rand_index_2[:, None]  # The generated size is (n, 1)
        for i in range(8):
            if i < 4:
                temp_enc = self.rand_index_1.T.dot(self.secret_key[i])  # Multiplicate the sizes: (1, n) . (n,
                    # n) to generate (1, n)
            else:
                temp_enc = self.rand_index_2.T.dot(self.secret_key[i])  # Multiplicate the sizes: (1, n) . (n,
                # n) to generate (1, n)
            self.encrypted_matrix.append(copy.deepcopy(temp_enc))


class Supplier(Person):
    """
    The User class contains methods that are only used by users, such as initiating query indexes. 
    """ 
    def __init__(self, supplier_secret_key, s, map, curr_pos: tuple, price_per_kw: float) -> None:
       
        Person.__init__(self, map.size + 1, supplier_secret_key, s, map, curr_pos) # AB: The added one in the first
        # parameter to justify the
        # padded value at the end that represents the price per KW
        self.price_per_kw = price_per_kw

    def calculate_cost(self):
        for pos_x in range(self.map.shape[0]):
            for pos_y in range(self.map[pos_x].shape[0]):
                self.map[pos_x,pos_y] = Person.calculate_euclidean_distance(self.curr_postion[0], self.curr_postion[
                    1], pos_x, pos_y)

    def encrypt(self):
        self.encrypted_matrix = []
        self.rand_index_1 = self.rand_index_1[:, None]  # The generated size is (n, 1)
        self.rand_index_2 = self.rand_index_2[:, None]  # The generated size is (n, 1)
        for i in range(8):
            if i < 4:
                temp_enc = self.secret_key[i].dot(self.rand_index_1)  # Multiplicate the sizes: (1, n) . (n,
                # n) to generate (1, n)
            else:
                temp_enc = self.secret_key[i].dot(self.rand_index_2)  # Multiplicate the sizes: (1, n) . (n,
                # n) to generate (1, n)
            self.encrypted_matrix.append(copy.deepcopy(temp_enc))

                    
class Server:
    """
    The Server class contains the query processing method, and return the matched documents
    """
    def __init__(self, x_inv: np.ndarray, y_inv: np.ndarray):
        self.__x_inv: np.ndarray = x_inv # Size (n, n)
        self.__y_inv: np.ndarray = y_inv # Size (n, n)

    def pre_process_received_demander_data(self, demanders: list[Demander]):
        """
        This function pre-processes the requests it receives from the demanders to save time in the processing.
        This function changes the encrypted_matrix inside the demander's list, as if the server has done its
        preprocessing over it when it receives it individually from the demander to save time when the matching
        algorithm runs
        """
        demander_ret_decrypted_list = []
        for pos_x in range(len(demanders)):
            for i in range(8):
                dem_enc = demanders[pos_x].encrypted_matrix[i]
                demanders[pos_x].encrypted_matrix[i] = dem_enc.dot(self.__x_inv).dot(self.__y_inv)

    def build_final_matrix(self, suppliers: list[Supplier], demanders: list[Demander]):
        self.final_matrix = np.zeros((len(demanders), len(suppliers)))
        for pos_x in range(len(demanders)):
            for pos_y in range(len(suppliers)):
                counter = 0
                for i in range(8):
                    dem_enc = demanders[pos_x].encrypted_matrix[i]
                    sup_enc = suppliers[pos_y].encrypted_matrix[i]
                    # sum_arr[i] = dem_enc.dot(self.__x_inv).dot(self.__y_inv).dot(sup_enc)
                    counter += dem_enc.dot(sup_enc)
                self.final_matrix[pos_x][pos_y] = counter

    def calcualte_assignment(self, cost_matrix):
         hungo = hung.Hungarian_algo(cost_matrix)
         self.ans, self.ans_mat = hungo.run_algo()


class KDC:
    """
    The Key Distribution Center (KDC) who is responsible for generating and distributing keys
    """

    def __init__(self):
        # Private Variables
        self.__kdc_master_key = None
        self.__server_key = None
        self.__demanders_secret_keys: list = None
        self.__suppliers_secret_keys: list = None

        # Public variables
        self.kdc_shared_split_vec = None

    def get_server_key(self):
        """
        This function should be called only by the server. The methodology to check that this method is called only
        by the server is out of our scope
        """
        return self.__server_key

    def get_supplier_secret_key(self, supplier_idx):
        """
        This function should be called only by a specific supplier to know his secret key. The methodology to check
        that this method is called only by the correct supplier is out of our scope
        """
        assert supplier_idx < len(self.__suppliers_secret_keys), "Supplier_idx ({}) is out of range of number of " \
                                                                 "suppliers ({})".format(supplier_idx, len(self.__suppliers_secret_keys))
        return self.__suppliers_secret_keys[supplier_idx]

    def get_demander_secret_key(self, demander_idx):
        """
        This function should be called only by a specific demander to know his secret key. The methodology to check
        that this method is called only by the correct demander is out of our scope
        """
        assert demander_idx < len(self.__demanders_secret_keys), "Supplier_idx ({}) is out of range of number of " \
                                                                 "demanders ({})".format(demander_idx, len(self.__demanders_secret_keys))
        return self.__demanders_secret_keys[demander_idx]

    def gen_keys(self, n: int, num_demanders: int, num_suppliers: int, generated_rand_num_start=1,
                 generated_ran_num_end=5, in_simulation_mode=False):
        """
        This function generates all the (public/private) keys needed by the server, the suppliers, and the demanders
        Inputs:
            - n: Matrices sizes as an int. All generated matrices will have the size (n, n)
            - num_demanders: Number of demanders
            - num_suppliers: Number of suppliers
            - generated_rand_num_start: The generated random numbers will start from this number
            - generated_ran_num_end: The generated random numbers will end at this number
        """
        assert generated_ran_num_end > generated_rand_num_start, "generated_ran_num_end ({}) should be greater than generated_rand_num_start ({})".format(generated_ran_num_end, generated_rand_num_start)

        X_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)  # TODO: AB:
        # Find a way to make sure that the randomization of this matrix has an inverse, and for all other random generated arrays as well
        Y_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        M1_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        M2_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N1_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N2_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N3_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N4_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N5_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N6_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N7_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        N8_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
        self.kdc_shared_split_vec = np.random.choice([0, 1], n)  # Shared with all demanders and suppliers

        x_rand_inv = np.linalg.inv(X_rand)
        y_rand_inv = np.linalg.inv(Y_rand)
        self.__server_key = (x_rand_inv, y_rand_inv)
        self.__kdc_master_key = [M1_rand, M2_rand, N1_rand, N2_rand, N3_rand, N4_rand, N5_rand, N6_rand, N7_rand, N8_rand]

        self.generate_demanders_keys(M1_rand, M2_rand, N1_rand, N2_rand, N3_rand, N4_rand, N5_rand, N6_rand, N7_rand,
                                     N8_rand, X_rand, generated_ran_num_end, generated_rand_num_start, n,
                                     num_demanders, in_simulation_mode)

        self.generate_suppliers_keys(M1_rand, M2_rand, N1_rand, N2_rand, N3_rand, N4_rand, N5_rand, N6_rand, N7_rand,
                                     N8_rand, Y_rand, generated_ran_num_end, generated_rand_num_start, n,
                                     num_suppliers, in_simulation_mode)

    def generate_suppliers_keys(self, M1_rand, M2_rand, N1_rand, N2_rand, N3_rand, N4_rand, N5_rand, N6_rand, N7_rand,
                                N8_rand, Y_rand, generated_ran_num_end, generated_rand_num_start, n, num_suppliers,
                                in_simulation_mode=False):
        suppliers_secret_keys = []
        M1_rand_inv = np.linalg.inv(M1_rand)
        M2_rand_inv = np.linalg.inv(M2_rand)
        N1_rand_inv = np.linalg.inv(N1_rand)
        N2_rand_inv = np.linalg.inv(N2_rand)
        N3_rand_inv = np.linalg.inv(N3_rand)
        N4_rand_inv = np.linalg.inv(N4_rand)
        N5_rand_inv = np.linalg.inv(N5_rand)
        N6_rand_inv = np.linalg.inv(N6_rand)
        N7_rand_inv = np.linalg.inv(N7_rand)
        N8_rand_inv = np.linalg.inv(N8_rand)

        num_suppliers = 1 if in_simulation_mode else num_suppliers # In simulation, all suppliers will use the same
        # key to save time
        for supplier_idx in range(num_suppliers):
            E_supplier_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
            F_supplier_rand = M1_rand_inv - E_supplier_rand

            G_supplier_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
            H_supplier_rand = M2_rand_inv - G_supplier_rand

            secret_key_term1 = Y_rand.dot(N1_rand_inv).dot(E_supplier_rand)
            secret_key_term2 = Y_rand.dot(N2_rand_inv).dot(F_supplier_rand)
            secret_key_term3 = Y_rand.dot(N3_rand_inv).dot(E_supplier_rand)
            secret_key_term4 = Y_rand.dot(N4_rand_inv).dot(F_supplier_rand)
            secret_key_term5 = Y_rand.dot(N5_rand_inv).dot(G_supplier_rand)
            secret_key_term6 = Y_rand.dot(N6_rand_inv).dot(H_supplier_rand)
            secret_key_term7 = Y_rand.dot(N7_rand_inv).dot(G_supplier_rand)
            secret_key_term8 = Y_rand.dot(N8_rand_inv).dot(H_supplier_rand)
            suppliers_secret_keys.append(
                [secret_key_term1, secret_key_term2, secret_key_term3, secret_key_term4, secret_key_term5,
                 secret_key_term6, secret_key_term7, secret_key_term8])
        self.__suppliers_secret_keys = suppliers_secret_keys

    def generate_demanders_keys(self, M1_rand, M2_rand, N1_rand, N2_rand, N3_rand, N4_rand, N5_rand, N6_rand, N7_rand,
                                N8_rand, X_rand, generated_ran_num_end, generated_rand_num_start, n, num_demanders,
                                in_simulation_mode=False):
        demanders_secret_keys = []
        num_demanders = 1 if in_simulation_mode else num_demanders # In simulation, all demanders will use the same
        # key to save time
        for demander_idx in range(num_demanders):
            A_demander_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
            B_demander_rand = M1_rand - A_demander_rand

            C_demander_rand = self.__generate_random_np_matrix(n, n, generated_rand_num_start, generated_ran_num_end)
            D_demander_rand = M2_rand - C_demander_rand

            secret_key_term1 = A_demander_rand.dot(N1_rand).dot(X_rand)
            secret_key_term2 = A_demander_rand.dot(N2_rand).dot(X_rand)
            secret_key_term3 = B_demander_rand.dot(N3_rand).dot(X_rand)
            secret_key_term4 = B_demander_rand.dot(N4_rand).dot(X_rand)
            secret_key_term5 = C_demander_rand.dot(N5_rand).dot(X_rand)
            secret_key_term6 = C_demander_rand.dot(N6_rand).dot(X_rand)
            secret_key_term7 = D_demander_rand.dot(N7_rand).dot(X_rand)
            secret_key_term8 = D_demander_rand.dot(N8_rand).dot(X_rand)
            demanders_secret_keys.append(
                [secret_key_term1, secret_key_term2, secret_key_term3, secret_key_term4, secret_key_term5,
                 secret_key_term6, secret_key_term7, secret_key_term8])
        self.__demanders_secret_keys = demanders_secret_keys

    def distribute_keys(self):
        """
        This function is used to return all the demanders secret keys, all the suppliers secret keys, and KDC shared
        split vector. This function should be used for debugging only, as in reality, each entity must acquire its
        secret key privately
        """
        return self.__demanders_secret_keys, self.__suppliers_secret_keys, self.kdc_shared_split_vec

    def __generate_random_np_matrix(self, num_rows: int, num_cols: int, generated_rand_num_start: int,
                                    generated_ran_num_end: int):
        return np.random.randint(generated_rand_num_start, generated_ran_num_end, (num_rows, num_cols))
