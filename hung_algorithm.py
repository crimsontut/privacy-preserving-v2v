import numpy as np
"""
    Code Author: @Mahmoud Srewa
    PhD student,  Computer Science Departement, the university of Alabama.
    Written in Alsharif Labs - the university of Alabama
"""
class Hungarian_algo:
    def __init__(self,intial_array: np.ndarray) -> None:
        self.intial_array = intial_array

  
    def subtract_min_row(self,arr: np.ndarray) -> np.ndarray:
        """
        this method operate for each row, it find the min value and subtract it from row
        arguments:
            arr: the array that we will operate on
        return:
            a new array after we apply the subtract operation
        """
        return arr - np.min(arr,axis=1)[:, np.newaxis]

    def subtract_min_coln(self,arr: np.ndarray) -> np.ndarray:
        """
        this method operate for each column, it find the min value and subtract it from coln
        arguments:
            arr: the array that we will operate on
        return:
            a new array after we apply the subtract operation
        """
        return arr - np.min(arr,axis=0)[:, np.newaxis].T

    def convert_into_binary(self,arr: np.ndarray) -> np.ndarray:
        """
        convert the matrix into binary one, 1 if a cell value is 0, 0 other wise
        arguments:
            arr: the array that we will operate on
        return:
            a new array after we apply
        """
        return np.where(arr==0, 1, 0)

    def min_row_with_zero(self,bool_mat, zero_marker) ->None:
        """
        this method tries to find the line with the min zero counts (1 in boolean matrix)
        after that it mark the first presence of 0 in that line.
        finally it change the entire row and coln of this marked 0 into 0
        arguments:
            bool_mat:    the array that we will operate on
            zero_marker:  a dictionary that its key is row, and value the coln which the zero lies
        return:
            No return, but it change value of bool_mat & the zero_marker
        """
        min_value = 1000000
        min_index = -1

        #np.maskedArray is used to operate only on the 1 value, and discarding the zeros
        min_row_index = np.argmin(np.sum(np.ma.MaskedArray(bool_mat, bool_mat<1),axis=1))
        
        #find the first absence of the 1 which later will be the marked zero
        marked_zero_index= np.where(bool_mat[min_row_index] == 1)[0][0]

        #adding the new marked zero to the dictionary 
        zero_marker[min_row_index] = marked_zero_index
        
        #now making the coln and row of the marked zero to be 0
        bool_mat[min_row_index,:] = 0
        bool_mat[:,marked_zero_index] = 0

    def find_min_rows_with_zero(self,arr:np.ndarray):
        """
        this method tries to find the marked zero coordinates, by calling @min_row_with_zero
        untill all the bool matrix values all be zero
        arguments:
            arr:    the array that we will operate on i.e, after applying the @subtract_min_row and
                    @subtract_min_coln
        return:
            zero_dec: the final dictionary that contians coordinate of the marked zeros
        """
        zero_dec = {}
        bool_mat = self.convert_into_binary(arr)
        #print(bool_mat)
        while 1 in bool_mat:
            self.min_row_with_zero(bool_mat, zero_dec)
        return zero_dec

    def hung_marking(self,arr:np.ndarray,zero_marker: dict ):
        """
        this method tries to find the lines and rows that covers all the zeros in the mat
        arguments:
            arr:         the array that we will operate on i.e, after applying the @subtract_min_row and
                         @subtract_min_coln
            zero_marker: dictionary that contians coordinate of the marked zeros
        return:
            zero_marker: the final dictionary that contians coordinate of the marked zeros
            rows_lines:  the row lines that covers the zero
            cols_lines:  the coln lines that covers the zero
        """
        #get boolean version of matrix 
        bool_mat_copy = self.convert_into_binary(arr)
        row_marked_zero = []
        coln_marked_zero = []

        #split both row and coln of every marked zero
        for key,value in zero_marker.items():
            row_marked_zero.append(key)
            coln_marked_zero.append(value)

        #find the non marked rows
        non_marked_rows = list(set(range(arr.shape[0])) - set(row_marked_zero))

        #define the final lines that covers all zeros
        cols_lines = []
        rows_lines = row_marked_zero.copy()


        flag=True
        while flag:
            flag = False
            #we iterate on the row of every non marked rows, to check whether it contains non marked zero
            for row in non_marked_rows:
                non_marked_row = bool_mat_copy[row,:]
                for coln in range(arr.shape[0]):
                    if non_marked_row[coln] == 1 and coln not in cols_lines:
                        #if exsist a value that not covered by a coln, add a new coln line to cover this zero
                        cols_lines.append(coln)
                        #flag = True

            #here we check if there exsist row covered a marked zero and in same time exsist the coln
            #that covers the same marked zero, we add to non marked rows
            for zero_x, zero_y in zero_marker.items():
                if zero_x not in non_marked_rows and zero_y in cols_lines:
                    non_marked_rows.append(zero_x)
                    rows_lines.remove(zero_x)
                    flag = True
        
        #rows_lines = list(set(range(arr.shape[0])) - set(non_marked_rows))

        return(zero_marker, rows_lines, cols_lines)

        
    
    def update_matrix(self, mat: np.ndarray,rows_lines: np.ndarray ,cols_lines: np.ndarray) -> np.ndarray:
        """
        this method operate whenever the sum of row & coln numbers does not match a the dimension of matrix
        if manipulate the values of matrix by finding the minimum of cells that not makred by any of lines.
        after that it subtract the minmuim value from all cell that are not marked by any lines.
        finally, we add the minmuim to the intersected cell between row & coln lines.

        arguments:
            mat:         the array that we will operate on i.e, after applying the @subtract_min_row and
                         @subtract_min_coln
            rows_lines:  the row lines that covers the zero
            cols_lines:  the coln lines that covers the zero
        return:
            mat       :  the result matrix after doing the operation
        """
        
        #to specifiy the cells which not covered by rows lines neither the colns lines
        #and then calculate its min. 
        not_marked_elements = []
        for row_index in range(mat.shape[1]):
            if row_index not in rows_lines:
                for coln_index in range(mat[row_index].size):
                    if coln_index not in cols_lines:
                      not_marked_elements.append(mat[row_index][coln_index])

        min_value = np.min(np.array(not_marked_elements))

        #subtract the calculated min from the cells which not covered by rows lines neither the colns lines
        for row_index in range(mat.shape[1]):
            if row_index not in rows_lines:
                for coln_index in range(mat[row_index].size):
                    if coln_index not in cols_lines:
                        mat[row_index][coln_index] = mat[row_index][coln_index] - min_value

        #add the calculated min to the intersected cell from two row & coln lines.
        for row in rows_lines:
            for coln in cols_lines:
               mat[row][coln] =   mat[row][coln] + min_value

        return mat

    def calculate_final_weight(self,mat:np.ndarray,zero_marker: dict ):
        """
        this method calculate the final weight of the assigned cell

        arguments:
            mat:         the array that we will operate on i.e, after applying the @subtract_min_row and
                         @subtract_min_coln
            zero_marker:  dictionary that contians coordinate of the marked zeros (final assignment soln)
        return:
            sum       :  the result weight of the algorithm
        """
        final_matrix = np.zeros(mat.shape)
        for x, y in zero_marker.items():
            final_matrix[x][y] = mat[x][y]
        return np.sum(final_matrix), final_matrix


    def run_algo(self):
        min_subtracted_row = self.subtract_min_row(self.intial_array)

        #we path the min_subtracted_row array  to subtract_min_coln() because we do row subtract first
        # and result is passed to column subtract
        min_subtracted_coln =self.subtract_min_coln(min_subtracted_row)


        #the zero_marker is a dictionary that simple represent coordinate of marked zeros which is a potentail ansewrs
        #later you will discover that 🕶
        zero_marker = self.find_min_rows_with_zero(min_subtracted_coln)

        #first of all the line count is zero, we didn't start yet :D
        lines_count = 0

        while lines_count !=self.intial_array.shape[0]:

            zero_marker, row_lines, cols_lines = self.hung_marking(min_subtracted_coln,
                zero_marker)

            # here we sum the rows & colns lines 
            lines_count = len(row_lines) + len(cols_lines)
            #print("Lines count",lines_count)

            # here we check again the lines count against dimension in order to change it
            # if its not match the condition 
            if lines_count !=  self.intial_array.shape[0]:

                #here we update the matrix
	            min_subtracted_coln = self.update_matrix(min_subtracted_coln, row_lines, cols_lines)

                # we recalculate the zero marker for the new matrix 
	            zero_marker = self.find_min_rows_with_zero(min_subtracted_coln)
        #print("final zero_marker: ",zero_marker)
        #print("final row_lines:   ",row_lines)
        #print("final cols_lines:  ",cols_lines)

        ans, ans_mat = self.calculate_final_weight(self.intial_array,zero_marker)
        #print("final cost:        ",ans)
        #print("answer Matrix:\n",ans_mat)
        return  ans, ans_mat

